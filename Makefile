maple: main.o utils.o
	g++ -std=c++11 main.o utils.o -o maple

main.o: main.cpp
	g++ -std=c++11 -c main.cpp

utils.o: utils.cpp utils.h
	g++ -std=c++11 -c utils.cpp

clean:
	rm *.o
