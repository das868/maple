#ifndef UTILS_H
#define UTILS_H

#include <iostream>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <random>
using namespace std;

int scalarProd(vector<int>, vector<int>);
vector<int> crossProd(vector<int>, vector<int>);
void printVect(vector<int>, vector<int>, char);

#endif
