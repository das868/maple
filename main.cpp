#include "utils.h"

int main(){
    //Start-up header
    printf(" Welcome to M.A.P.L.E – Math A-levels Preparator (Language Enhanced)\n");
    printf("=====================================================================\n\n");
    printf("Main menu.\n");
    printf("\t1\tDot Product\n");
    printf("\t2\tCross Product\n");
    printf("TO BE CONTINUED...\n\n");
    printf("Your choice:\t");
    int choice = -1;
    string helpstr;
    getline(cin, helpstr);
    choice = stoi(helpstr);
    printf("Enter number of reps:\t");
    int reps = -1;
    getline(cin, helpstr);
    reps = stoi(helpstr);
    int rightscounter, guessed;
    vector<int> u = {0, 0, 0};
    vector<int> v = {0, 0, 0};
    int result = -1;
    vector<int> guessedVect = {0, 0, 0};
    vector<int> resultVect = {0, 0, 0};
    // Random number generator based on equal distribution -- Thanks to 
    //https://stackoverflow.com/questions/13445688/how-to-generate-a-random-number-in-c#13445752
    mt19937 randNumGen;
    randNumGen.seed(random_device()());
    uniform_int_distribution<mt19937::result_type> dist(-9, 9);
    switch(choice){
        case 1:
            {
                for(int i=0; i<reps; i++){
                    u[0] = dist(randNumGen);
                    u[1] = dist(randNumGen);
                    u[2] = dist(randNumGen);
                    v[0] = dist(randNumGen);
                    v[1] = dist(randNumGen);
                    v[2] = dist(randNumGen);
                    //printf("Size of u=%i, of v=%i", u.size(), v.size());
                    printVect(u, v, 'o');
                    printf("\t=\t");
                    getline(cin, helpstr);
                    guessed = stoi(helpstr);
                    result = scalarProd(u, v);
                    if(guessed == result){
                        printf("Correct.\n");
                        rightscounter++;
                    }
                    else{
                        printf("Wrong, right answer: %i\n", result);
                    }
                }
                break;
            }
        case 2:
            {
                for(int i=0; i<reps; i++){
                    u[0] = dist(randNumGen);
                    u[1] = dist(randNumGen);
                    u[2] = dist(randNumGen);
                    v[0] = dist(randNumGen);
                    v[1] = dist(randNumGen);
                    v[2] = dist(randNumGen);
                    printVect(u, v, 'x');
                    printf("\t\b\b\b\bx = ");
                    getline(cin, helpstr);
                    guessedVect[0] = stoi(helpstr);
                    printf("\t\b\b\b\by = ");
                    getline(cin, helpstr);
                    guessedVect[1] = stoi(helpstr);
                    printf("\t\b\b\b\bz = ");
                    getline(cin, helpstr);
                    guessedVect[2] = stoi(helpstr);
                    resultVect = crossProd(u, v);
                    if((guessedVect[0] == resultVect[0]) && (guessedVect[1] == resultVect[1]) && (guessedVect[2] == resultVect[2])){
                        printf("Correct.\n");
                        rightscounter++;
                    }
                    else{
                        printf("Wrong, right answer: (%i, %i, %i)^T\n", resultVect[0], resultVect[1], resultVect[2]);
                    }
                }
                break;
            }
        default:
            printf("ERROR – Unable to match choice to given alternatives, quitting...\n");
            break;
    }
    double percentage = (double)rightscounter/(double)reps;
    int grade = -1;
    if(percentage >= 0.96){
        grade = 15;
    }
    else if(percentage >= 0.91){
        grade = 14;
    }
    else if(percentage >= 0.86){
        grade = 13;
    }
    else if(percentage >= 0.81){
        grade = 12;
    }
    else if(percentage >= 0.76){
        grade = 11;
    }
    else if(percentage >= 0.71){
        grade = 10;
    }
    else if(percentage >= 0.66){
        grade = 9;
    }
    else if(percentage >= 0.61){
        grade = 8;
    }
    else if(percentage >= 0.56){
        grade = 7;
    }
    else if(percentage >= 0.51){
        grade = 6;
    }
    else if(percentage >= 0.46){
        grade = 5;
    }
    else if(percentage >= 0.41){
        grade = 4;
    }
    else if(percentage >= 0.36){
        grade = 3;
    }
    else if(percentage >= 0.31){
        grade = 2;
    }
    else if(percentage >= 0.26){
        grade = 1;
    }
    else{
        grade = 0;
    }
    printf("You have completed %i of %i reps right, totalling %f%% = grade %i/15.\n", rightscounter, reps, 100*percentage, grade);
    return 0;
}
