#include "utils.h"

int scalarProd(vector<int> u, vector<int> v){
    int result = 0;
    if(u.size() == v.size()){
        for(unsigned int i=0; i<u.size(); i++){
            result += u[i] * v[i];
        }
    }
    else{
        printf("ERROR – vectors not at equivalent size, quitting...");
    }
    return result;
}

vector<int> crossProd(vector<int> u, vector<int> v){
    vector<int> result = {0, 0, 0};
    if((u.size() == 3) && (v.size() == 3)){
        result[0] = u[1] * v[2] - u[2] * v[1];
        result[1] = u[2] * v[0] - u[0] * v[2];
        result[2] = u[0] * v[1] - u[1] * v[0];
    }
    else{
        printf("ERROR – vectors not at equivalent size, quitting...");
    }
    return result;
}

void printVect(vector<int> u, vector<int> v, char symb){
    if((u.size() == 3) && (v.size() == 3)){
        string strs[6];
        bool areAllNonNeg = (u[0] >= 0) && (u[1] >= 0) && (u[2] >= 0) && (v[0] >= 0) && (v[1] >= 0) && (v[2] >= 0);
        for(unsigned int i=0; i<(u.size() + v.size()); i++){
            if(i<3){
                if((u[i] < 0) || areAllNonNeg){
                    strs[i] = to_string(u[i]);
                }
                else{
                    strs[i] = " " + to_string(u[i]);
                }
            }
            else{
                if((v[i-3] < 0) || areAllNonNeg){
                    strs[i] = to_string(v[i-3]);
                }
                else{
                    strs[i] = " " + to_string(v[i-3]);
                }
            }
        }
        cout << "\n\t/" << strs[0] << " \\   /" << strs[3] << " \\" << endl
             << "\t|" << strs[1] << " | " << symb << " |" << strs[4] << " |" << endl
             << "\t\\" << strs[2] << " /   \\" << strs[5] << " /" << endl;
    }
    else{
        printf("ERROR – vector sizes not matching up, quitting...");
    }
}
